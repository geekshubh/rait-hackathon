<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class TowerRepair extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'repair:towers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $repair = "";
      $users = User::where('id','=', $assigned_to)->get();
      Notification::send($users, new TowerRepairNotification($tower));
    }
}

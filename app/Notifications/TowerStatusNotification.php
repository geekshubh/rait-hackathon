<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Twilio\TwilioChannel;
use NotificationChannels\Twilio\TwilioSmsMessage;

class TowerStatusNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $tower,$msg;
    public function __construct($tower)
    {
        $this->tower = $tower;
        $msg = "The Tower status is currently at ".$tower->status."% and was last checked on: ".$tower->last_repaired;
        $this->msg = $msg;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', TwilioChannel::class];
    }

    public function toTwilio($notifiable)
    {
        return(new TwilioSmsMessage())->content($this->msg);
    }

    public function toMail($notifiable)
    {
      $url = "https://hackathon.e-ducate.in/";
      return (new MailMessage)
                  ->line('Tower Status')
                  ->action('Go to website', $url)
                  ->line('Thank you for using our application!');
    }
    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

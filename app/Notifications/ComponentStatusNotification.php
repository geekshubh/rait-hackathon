<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Twilio\TwilioChannel;
use NotificationChannels\Twilio\TwilioSmsMessage;

class ComponentStatusNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $component,$msg;
    public function __construct($component)
    {
      $this->component = $component;
      $msg = "The Component ".$component->title." and its status is currently at ".$component->status."% and was last replaced on: ".$component->last_replaced." and it's expiry date is: ".$component->expiry_date;
      $this->msg = $msg;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', TwilioChannel::class];
    }
    public function toTwilio($notifiable)
    {
        return(new TwilioSmsMessage())->content($this->msg);
    }
    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
     public function toMail($notifiable)
     {
       $url = "https://hackathon.e-ducate.in/";
       return (new MailMessage)
                   ->line('Component Status')
                   ->action('Go to website', $url)
                   ->line('Thank you for using our application!');
     }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Twilio\TwilioChannel;
use NotificationChannels\Twilio\TwilioSmsMessage;

class TowerWeatherStatusNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $tower,$msg,$weather_main,$weather_desc;
    public function __construct($tower,$weather_main,$weather_desc)
    {
      $this->tower = $tower;
      $msg = "The Weather status is currently at ".$tower->title." is: ".$weather_main. " and is described as: ".$weather_desc.". Please take necessary action if required";
      $this->msg = $msg;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail',TwilioChannel::class];
    }

    public function toTwilio($notifiable)
    {
        return(new TwilioSmsMessage())->content($this->msg);
    }
    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
     public function toMail($notifiable)
     {
       $url = "https://hackathon.e-ducate.in/";
       return (new MailMessage)
                   ->line('Tower Weather Status')
                   ->action('Go to website', $url)
                   ->line('Thank you for using our application!');
     }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

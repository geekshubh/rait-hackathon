<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Twilio\TwilioChannel;
use NotificationChannels\Twilio\TwilioSmsMessage;

class ComponentRepairStatusNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $component,$msg,$tower;
    public function __construct($component,$tower)
    {
      $this->component = $component;
      $this->tower  = $tower;
      $msg = "The Component status is currently at ".$component->status."% and it is a part of tower: ".$tower->title." of group ".$tower->group_id. "and it's google map link is: http://maps.google.com/?ll=".$tower->add_lat.",".$tower->add_lng;
      $this->msg = $msg;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail',TwilioChannel::class];
    }
    public function toTwilio($notifiable)
    {
        return(new TwilioSmsMessage())->content($this->msg);
    }
    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
     public function toMail($notifiable)
     {
       $url = "https://hackathon.e-ducate.in/";
       return (new MailMessage)
                   ->line('Component Needs Repair')
                   ->action('Go to website', $url)
                   ->line('Thank you for using our application!');
     }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

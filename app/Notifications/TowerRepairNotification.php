<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Twilio\TwilioChannel;
use NotificationChannels\Twilio\TwilioSmsMessage;

class TowerRepairNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $tower,$msg;
    public function __construct($tower)
    {
      $this->tower = $tower;
      $msg = "The Tower ".$tower->title." status is currently at ".$tower->status."% and was last checked on: ".$tower->last_repaired."Please visit the area as soon as possible using following link: https://www.google.com/maps/search/?api=1&query=".$tower->add_lat.",".$tower->add_lng;
      $this->msg = $msg;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', TwilioChannel::class];
    }

    public function toTwilio($notifiable)
    {
        return(new TwilioSmsMessage())->content($this->msg);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
     public function toMail($notifiable)
     {
       $url = "https://hackathon.e-ducate.in/";
       return (new MailMessage)
                   ->line('Tower repair is required')
                   ->action('Go to website', $url)
                   ->line('Thank you for using our application!');
     }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

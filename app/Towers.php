<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Towers extends Model
{
  protected $table = "towers";
  protected $fillable = ['group_id','title','description','status','last_repaired','next_repair','assigned_to'];

  public function assigned_to_user()
  {
    return $this->belongsTo('App\User','assigned_to')->withTrashed();
  }

  public function groups()
  {
    return $this->belongsTo('App\TowerGroups','group_id');
  }

  public function components()
  {
    return $this->hasMany('App\Towers','tower_id');
  }
}

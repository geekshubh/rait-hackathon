<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TowerGroups extends Model
{
  protected $table = "tower_groups";
  protected $fillable = ['title','description','uid'];

  public function groups()
  {
    return $this->hasMany('App\TowerGroups','group_id')->withTrashed();
  }
}

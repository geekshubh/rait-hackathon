<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class DayOfWeek extends Model
{
  use SoftDeletes;
  protected $table = "day_of_week";
  protected $fillable = ['title'];
}

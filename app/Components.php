<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Components extends Model
{
    protected $table = "components";
    protected $fillable = ['title','tower_id','description','last_replaced','operational_date','expiry_date','status'];

    public function components()
    {
      return $this->belongsTo('App\Towers','tower_id');
    }
}

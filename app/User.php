<?php
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Hash;
use Mail;

class User extends Authenticatable implements JWTSubject
{
  use SoftDeletes,Notifiable;
  protected $fillable = ['name', 'email', 'password','remember_token', 'role_id','phone_number','gender','uid_no','profile_pic'];

  public static function boot()
  {
    parent::boot();
    User::observe(new \App\Observers\UserActionsObserver);
  }

  public function setPasswordAttribute($input)
  {
    if ($input)
    {
      $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }
  }

  public function setRoleIdAttribute($input)
  {
    $this->attributes['role_id'] = $input ? $input : null;
  }

  public function role()
  {
    return $this->belongsTo('App\Role', 'role_id')->withTrashed();
  }

  public function assigned_to_user()
  {
    return $this->hasMany('App\User','assigned_to')->withTrashed();
  }

  public function isAdmin()
  {
    foreach ($this->role()->get() as $role)
    {
      if ($role->id == 1)
      {
        return true;
      }
    }
    return false;
  }

  public function isOfficeStaff()
  {
    foreach ($this->role()->get() as $role)
    {
      if ($role->id == 2)
      {
        return true;
      }
    }
    return false;
  }

  public function isFieldOfficer()
  {
    foreach ($this->role()->get() as $role)
    {
      if ($role->id == 3)
      {
        return true;
      }
    }
    return false;
  }

  protected $hidden =
  [
    'password', 'remember_token',
  ];

  public function getJWTIdentifier()
  {
    return $this->getKey();
  }

  public function getJWTCustomClaims()
  {
    return [];
  }
}

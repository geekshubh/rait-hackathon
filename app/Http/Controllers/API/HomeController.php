<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Question;
use App\Result;
use App\Test;
use App\User;
use Illuminate\Http\Request;
use App\StudentParents;
use Auth;
use App\Assignments;
use DB;
use App\AttendanceStatus;
use App\Attendance;
use App\EducationalMaterials;
use App\LibraryBooks;
use App\ClassroomTimetable;
use App\Classroom;
use Validator;
use JWTFactory;
use JWTAuth;

class HomeController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index()
  {
    if(Auth::user()->isStudent())
    {
      $assignments = Assignments::where('classroom_id',Auth::user()->student_classroom_id)->orderBy('deadline','asc')->take(5)->get();
      $attendance = AttendanceStatus::where('student_id',Auth::id())->orderBy('created_at','desc')->take(5)->get();
      $materials = EducationalMaterials::where('classroom_id', Auth::user()->student_classroom_id)->orderBy('created_at','desc')->take(5)->get();
      $library_books = LibraryBooks::where('assigned_to', Auth::id())->orderBy('due_date', 'desc')->take(5)->get();
      return response()->json(compact('assignments','attendance','materials','library_books'),200);
    }
    elseif(Auth::user()->isTeacher())
    {
      $assignments = Assignments::where('added_by',Auth::id())->orderBy('deadline','asc')->take(5)->get();
      $materials = EducationalMaterials::where('added_by',Auth::id())->take(5)->get();
      $timetable = ClassroomTimetable::where('teacher_id',Auth::id())->get();
      $library_books = LibraryBooks::where('assigned_to', Auth::id())->orderBy('due_date', 'desc')->take(5)->get();
      return response()->json(compact('assignments','materials','library_books','timetable'),200);
    }
    elseif(Auth::user()->isAdmin())
    {
      $assignments = Assignments::orderBy('deadline','desc')->take(5)->get();
      $materials = EducationalMaterials::orderBy('created_at','desc')->take(5)->get();
      $library_books = LibraryBooks::orderBy('due_date', 'desc')->take(5)->get();
      $classrooms = Classroom::all();
      return response()->json(compact('assignments','materials','library_books','classrooms'),200);
    }
    else
    {
      return response()->json();
    }
  }
}

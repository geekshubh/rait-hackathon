<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UserAction;
use App\Http\Requests\StoreUserActionsRequest;
use App\Http\Requests\UpdateUserActionsRequest;
use Auth;
use Validator;
use JWTFactory;
use JWTAuth;

class UserActionsController extends Controller
{
  public function index()
  {
    if(Auth::user()->isAdmin())
    {
      $user_actions = UserAction::all();
      return response()->json(compact('user_actions'),200);
    }
    else
    {
      return response()->json(['errors' => 'You are not allowed to View user actions'], 401);
    }
  }
}

<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\Http\Requests\StoreUsersRequest;
use App\Http\Requests\UpdateUsersRequest;
use App\CsvData;
use App\Http\Requests\StoreCsvImportRequest;
use Maatwebsite\Excel\Facades\Excel;
use Auth;
use Validator;
use JWTFactory;
use JWTAuth;
class UsersController extends Controller
{
  public function index()
  {
    if(Auth::user()->isAdmin())
    {
      $users = User::all();
      return response()->json(compact('users'),200);
    }
    else
    {
      return response()->json(['errors'=>'You are not allowed to view the Requested Page'],401);
    }
  }

  public function create()
  {
    if(Auth::user()->isAdmin())
    {
      $roles = Role::get()->pluck('title', 'id')->prepend('Please select', '');
      $gender = ['Male'=>'Male','Female'=>'Female','Others'=>'Others'];
      return response()->json(compact('roles','gender'),200);
    }
    else
    {
      return response()->json(['errors'=>'You are not allowed to Add Users']);
    }
  }

  public function store(StoreUsersRequest $request)
  {
    if(Auth::user()->isAdmin())
    {
      // Handle File Upload
      if($request->hasFile('profile_pic'))
      {
        // Get filename with the extension
        $filenameWithExt = $request->file('profile_pic')->getClientOriginalName();
        // Get just filename
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        // Get just ext
        $extension = $request->file('profile_pic')->getClientOriginalExtension();
        // Filename to store
        $fileNameToStore= $filename.'_'.time().'.'.$extension;
        // Upload Image
        $path = $request->file('profile_pic')->storeAs('public/profile-pics', $fileNameToStore);
      }
      else
      {
        $fileNameToStore = '';
      }
      $users = new User;
      $users->name = $request->input('name');
      $users->email = $request->input('email');
      $users->password = $request->input('password');
      $users->roll_no = $request->input('roll_no');
      $users->phone_number = $request->input('phone_number');
      $users->gender = $request->input('gender');
      $users->role_id = $request->input('role_id');
      $users->profile_pic = $fileNameToStore;
      $users->save();
      return response()->json(['success'=>'User Added Successfully'],200);
    }
    else
    {
      return response()->json(['errors'=>'You are not allowed to Add Users'],401);
    }
  }

  public function edit($id)
  {
    if(Auth::user()->isAdmin())
    {
      $roles = Role::get()->pluck('title', 'id')->prepend('Please select', '');
      $user = User::findOrFail($id);
      $gender = ['Male'=>'Male','Female'=>'Female','Others'=>'Others'];
      return response()->json(compact('user','roles','gender'),200);
    }
    else
    {
      return response()->json(['errors'=>'You are not allowed to Edit User'],401);    }
  }

  public function update(UpdateUsersRequest $request, $id)
  {
    if(Auth::user()->isAdmin())
    {
      if($request->hasFile('profile_pic'))
      {
        // Get filename with the extension
        $filenameWithExt = $request->file('profile_pic')->getClientOriginalName();
        // Get just filename
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        // Get just ext
        $extension = $request->file('profile_pic')->getClientOriginalExtension();
        // Filename to store
        $fileNameToStore= $filename.'_'.time().'.'.$extension;
        // Upload Image
        $path = $request->file('profile_pic')->storeAs('public/profile-pics', $fileNameToStore);
      }
      else
      {
        $fileNameToStore = '';
      }
      $users = User::findOrFail($id);
      $users->name = $request->input('name');
      $users->email = $request->input('email');
      $users->password = $request->input('password');
      $users->roll_no = $request->input('roll_no');
      $users->phone_number = $request->input('phone_number');
      $users->gender = $request->input('gender');
      $users->role_id = $request->input('role_id');
      if($request->hasFile('profile_pic'))
      {
        $users->profile_pic = $fileNameToStore;
      }
      $users->save();
      return redirect()->json(['success'=>'User Updated Successfully'],200);
    }
    else
    {
      return response()->json(['errors'=>'You are not allowed to Edit User'],401);
    }
  }

  public function show($id)
  {
    if(Auth::user()->isAdmin())
    {
      $users = User::findOrFail($id);
      return response()->json(compact('users'));
    }
    else
    {
      return response()->json(['errors'=>'You are not allowed to View the User'],401);
    }
  }

  public function destroy($id)
  {
    if(Auth::user()->isAdmin())
    {
      $user = User::findOrFail($id);
      $user->delete();
      return response()->json(['success'=>'User Deleted Succesfully'],200);
    }
    else
    {
      return response()->json(['errors'=>'You are not allowed to Delete the User'],401);
    }
  }
  // This function returns the initial import page
  public function getImport()
  {
    if(Auth::user()->isAdmin())
    {
      return response()->json(['success'=>'You are allowed to Import Users'],200);
    }
    else
    {
      return response()->json(['errors'=>'You are not allowed to Import Users'],401);
    }
  }

  // This function saves the data initially in json format
  public function parseImport(Request $request)
  {
    if(Auth::user()->isAdmin())
    {
      $path = $request->file('csv_file')->getRealPath();
      if ($request->has('header'))
      {
        $data = Excel::load($path, function($reader) {})->get()->toArray();
      }
      else
      {
        $data = array_map('str_getcsv', file($path));
      }
      if (count($data) > 0)
      {
        if ($request->has('header'))
        {
          $csv_header_fields = [];
          foreach ($data[0] as $key => $value)
          {
            $csv_header_fields[] = $key;
          }
        }
        $csv_data = array_slice($data, 0, 2);
        $csv_data_file = CsvData::create([
          'csv_filename' => $request->file('csv_file')->getClientOriginalName(),
          'csv_header' => $request->has('header'),
          'csv_data' => json_encode($data)
        ]);
      }
      else
      {
        return redirect()->back();
      }
      return response()->json(compact('csv_header_fields','csv_data','csv_data_file'));
    }
    else
    {
      return response()->json(['errors'=>'You are not allowed to Import Users'],401);
    }
  }

  // This function saves the data in the database
  public function processImport(Request $request)
  {
    if(Auth::user()->isAdmin())
    {
      $data = CsvData::find($request->csv_data_file_id);
      $csv_data = json_decode($data->csv_data, true);
      foreach ($csv_data as $row)
      {
        $users = new User();
        foreach (config('import.users_db_fields') as $index => $field)
        {
          if ($data->csv_header)
          {
            $users->$field = $row[$request->fields[$field]];
          }
          else
          {
            $users->$field = $row[$request->fields[$index]];
          }
        }
        $users->save();
      }
      return response()->json(['success'=>'User Imported Successfully'],200);
    }
    else
    {
      return response()->json(['errors'=>'You are not allowed to Import Users'],401);
    }
  }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TowerGroups;
use App\Towers;
use App\Components;
use App\User;
use Auth;
use Notification;
use App\Notifications\ComponentStatusNotification;
use App\Notifications\ComponentRepairStatusNotification;

class ComponentsController extends Controller
{

  public function index($tower_id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff() || Auth::user()->isFieldOfficer())
    {
      $components = Components::where('tower_id',$tower_id)->get();
      return view('components.index',compact('components','tower_id'));
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to access this module');
    }
  }

  public function create($tower_id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff() || Auth::user()->isFieldOfficer())
    {
      $towers = Towers::all()->pluck('title','id');
      return view('components.create',compact('towers','tower_id'));
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to access this module');
    }
  }

  public function store(Request $request,$tower_id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff() || Auth::user()->isFieldOfficer())
    {
      $components = new Components;
      $components->tower_id = $tower_id;
      $components->title = $request->title;
      $components->description = $request->description;
      $components->last_replaced = $request->last_replaced;
      $components->operational_date = $request->operational_date;
      $components->expiry_date = $request->expiry_date;
      $components->status = $request->status;
      $components->save();
      return redirect()->route('components.index',[$tower_id])->with('success','Component Added Successfully');
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to access this module');
    }
  }

  public function edit($tower_id,$id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff() || Auth::user()->isFieldOfficer())
    {
      $components = Components::findOrFail($id);
      $towers = Towers::all()->pluck('title','id');
      return view('components.edit',compact('components','towers','tower_id'));
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to access this module');
    }
  }

  public function update(Request $request,$tower_id,$id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff() || Auth::user()->isFieldOfficer())
    {
      $components = Components::findOrFail($id);
      $components->tower_id = $tower_id;
      $components->title = $request->title;
      $components->description = $request->description;
      $components->last_replaced = $request->last_replaced;
      $components->operational_date = $request->operational_date;
      $components->expiry_date = $request->expiry_date;
      $components->status = $request->status;
      $components->update();
      if($request->status <= 25)
      {
        $component = Components::findOrFail($id);
        $tower = Towers::findOrFail($tower_id);
        $assigned_to = Towers::where('id',$tower_id)->pluck('assigned_to')->first();
        $users = User::where('id','=', $assigned_to)->get();
        Notification::send($users, new ComponentStatusNotification($component,$tower));
        return redirect()->route('components.index',[$tower_id])->with('success','Notification Sent Successfully');
      }
      return redirect()->route('components.index',[$tower_id])->with('success','Component Updated Successfully');
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to access this module');
    }
  }

  public function destroy($tower_id,$id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff() || Auth::user()->isFieldOfficer())
    {
      $components = Components::findOrFail($id);
      $components->delete();
      return redirect()->route('components.index',[$tower_id])->with('success','Component Deleted Successfully');
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to access this module');
    }
  }

  public function show($tower_id,$id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff() || Auth::user()->isFieldOfficer())
    {
      $component = Components::findOrFail($id);
      return view('components.show',compact('component','tower_id'));
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to access this module');
    }
  }
  public function notification($tower_id,$id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff())
    {
      $components = Components::findOrFail($id);
      $assigned_to = Towers::where('id',$tower_id)->pluck('assigned_to')->first();
      $users = User::where('id','=', $assigned_to)->get();
      Notification::send($users, new ComponentStatusNotification($components));
      return redirect()->route('components.index',[$tower_id])->with('success','Notification Sent Successfully');
    }
    else
    {
      return redirect()->back()->with('errors','Error');
    }
  }
}

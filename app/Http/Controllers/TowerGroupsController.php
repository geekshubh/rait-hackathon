<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TowerGroups;
use Auth;
use App\Http\Requests\StoreTowerGroupsRequest;
use App\Http\Requests\UpdateTowerGroupsRequest;

class TowerGroupsController extends Controller
{
  public function index()
  {
    if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff())
    {
      $groups = TowerGroups::all();
      //dd($groups);
      return view('tower-groups.index',compact('groups'));
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to access this module');
    }
  }

  public function create()
  {
    if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff())
    {
      return view('tower-groups.create');
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to access this module');
    }
  }

  public function store(StoreTowerGroupsRequest $request)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff())
    {
      $groups = new TowerGroups;
      $groups->title = $request->title;
      $groups->uid = $request->uid;
      $groups->description = $request->description;
      $groups->save();
      return redirect()->route('tower-groups.index')->with('success','Tower Group Successfully Added');
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to access this module');
    }
  }

  public function edit($id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff())
    {
      $groups = TowerGroups::findOrFail($id);
      return view('tower-groups.edit',compact('groups'));
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to access this module');
    }
  }

  public function update(UpdateTowerGroupsRequest $request,$id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff())
    {
      $groups = TowerGroups::findOrFail($id);
      $groups->title = $request->title;
      $groups->uid = $request->uid;
      $groups->description = $request->description;
      $groups->update();
      return redirect()->route('tower-groups.index')->with('success','Tower Group Successfully Updated');
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to access this module');
    }
  }

  public function show($id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff())
    {
      $group = TowerGroups::findOrFail($id);
      return view('tower-groups.show',compact('group'));
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to access this module');
    }
  }

  public function destroy($id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff())
    {
      $groups = TowerGroups::findOrFail($id);
      $groups->delete();
      return redirect()->route('tower-groups.index')->with('success','Tower Group Successfully Updated');
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to access this module');
    }
  }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class TutorialController extends Controller
{

  // User-Actions
  public function user_actions_index()
  {
    if(Auth::user()->isAdmin())
    {
      return view('tutorials.user-actions.index');
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to view this Tutorial');
    }
  }

  // Users
  public function users_index()
  {
    if(Auth::user()->isAdmin() || Auth::user()->isPrincipal())
    {
      return view('tutorials.users.index');
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to view this Tutorial');
    }
  }
  public function users_create()
  {
    if(Auth::user()->isAdmin())
    {
      return view('tutorials.users.create');
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to view this Tutorial');
    }
  }
  public function users_edit()
  {
    if(Auth::user()->isAdmin())
    {
      return view('tutorials.users.edit');
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to view this Tutorial');
    }
  }
  public function users_edit_profile()
  {
    if(Auth::user()->isAdmin())
    {
      return view('tutorials.users.edit_profile');
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to view this Tutorial');
    }
  }
  public function users_view()
  {
    if(Auth::user()->isAdmin()|| Auth::user()->isPrincipal())
    {
      return view('tutorials.users.view');
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to view this Tutorial');
    }
  }
  public function users_import()
  {
    if(Auth::user()->isAdmin())
    {
      return view('tutorials.users.import');
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to view this Tutorial');
    }
  }
  public function users_import_fields()
  {
    if(Auth::user()->isAdmin())
    {
      return view('tutorials.users.import_fields');
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to view this Tutorial');
    }
  }
  public function users_profile()
  {
    if(Auth::user()->isAdmin())
    {
      return view('tutorials.users.profile');
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to view this Tutorial');
    }
  }
}

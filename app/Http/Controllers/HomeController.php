<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;
use App\Towers;
use App\TowerGroups;
use App\Components;
class HomeController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index()
  {
      $towers = Towers::all()->count();
      $components = Components::all()->count();
      $tower_groups = TowerGroups::all()->count();
      $users = User::all()->count();
      $critical_towers = Towers::where('status','<=',25)->get();
      $critical_components = Components::where('status','<',25)->get();
      //dd($critical_components);
      return view('home',compact('towers','components','tower_groups','users','critical_towers','critical_components'));
  }
}

<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateUserProfileRequest;

class ProfileController extends Controller
{
  // This function gets the user and returns the profile page
  public function index()
  {
    if(Auth::user()->isAdmin() || Auth::user()->isPrincipal() || Auth::user()->isTeacher() || Auth::user()->isParent() || Auth::user()->isStudent() || Auth::user()->isAccountant() || Auth::user()->isLibrarian() || Auth::user()->isOfficeStaff())
    {
      $users = Auth::user();
      return view('users.profile', compact('users'));
    }
    else
    {
      return redirect()->back()->with('errors',trans('controller.profile.view_error'));
    }
  }
  // This function shows the edit page for editing the profile
  public function edit($id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isPrincipal() || Auth::user()->isTeacher() || Auth::user()->isParent() || Auth::user()->isStudent() || Auth::user()->isAccountant() || Auth::user()->isLibrarian() || Auth::user()->isOfficeStaff())
    {
      $user = User::findOrFail($id);
      $gender = ['Male'=>'Male','Female'=>'Female','Others'=>'Others'];
      return view('users.edit_profile', compact('user','gender'));
    }
    else
    {
      return redirect()->back()->with('errors',trans('controller.profile.edit_error'));
    }
  }

  // Update the user data in the table
  public function update(UpdateUserProfileRequest $request, $id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isPrincipal() || Auth::user()->isTeacher() || Auth::user()->isParent() || Auth::user()->isStudent() || Auth::user()->isAccountant() || Auth::user()->isLibrarian() || Auth::user()->isOfficeStaff())
    {
      if($request->hasFile('profile_pic'))
      {
        // Get filename with the extension
        $filenameWithExt = $request->file('profile_pic')->getClientOriginalName();
        // Get just filename
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        // Get just ext
        $extension = $request->file('profile_pic')->getClientOriginalExtension();
        // Filename to store
        $fileNameToStore= $filename.'_'.time().'.'.$extension;
        // Upload Image
        $path = $request->file('profile_pic')->storeAs('public/profile-pics', $fileNameToStore);
      }
      else
      {
        $fileNameToStore = '';
      }
      $users = User::findOrFail($id);
      $users->name = $request->input('name');
      $users->email = $request->input('email');
      $users->password = $request->input('password');
      $users->phone_number = $request->input('phone_number');
      $users->address = $request->input('address');
      $users->gender = $request->input('gender');
      $users->dob = $request->input('dob');
      if($request->hasFile('profile_pic'))
      {
        $users->profile_pic = $fileNameToStore;
      }
      $users->save();
      return redirect()->route('users.profile')->with('success', 'Profile Updated');
    }
    else
    {
      return redirect()->back()->with('errors',trans('controller.profile.edit_error'));
    }
  }
}

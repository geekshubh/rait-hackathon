<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TowerGroups;
use App\Towers;
use App\User;
use Auth;
use App\Http\Requests\StoreTowersRequest;
use App\Http\Requests\UpdateTowersRequest;
use Notification;
use App\Notifications\TowerStatusNotification;
use App\Notifications\TowerRepairNotification;
use GuzzleHttp\Client;
use App\Notifications\TowerWeatherStatusNotification;

class TowersController extends Controller
{
  public function index()
  {
    if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff() || Auth::user()->isFieldOfficer())
    {
      $towers = Towers::all();
      return view('towers.index',compact('towers'));
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to access this module');
    }
  }

  public function create()
  {
    if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff())
    {
      $groups = TowerGroups::all()->pluck('title','id');
      $officers = User::where('role_id',3)->pluck('name','id');
      return view('towers.create',compact('groups','officers'));
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to access this module');
    }
  }

  public function store(Request $request)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff())
    {
      $tower = new Towers;
      $tower->title = $request->title;
      $tower->group_id = $request->group_id;
      $tower->description = $request->description;
      $tower->status = $request->status;
      $tower->last_repaired = $request->last_repaired;
      $tower->next_repair = $request->next_repair;
      $tower->assigned_to = $request->assigned_to;
      $tower->address = $request->address;
      $tower->add_lat = $request->add_lat;
      $tower->add_lng = $request->add_lng;
      $tower->save();
      return redirect()->route('towers.index')->with('success','Tower has been added successfully');
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to access this module');
    }
  }

  public function edit($id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff() || Auth::user()->isFieldOfficer())
    {
      $tower = Towers::findOrFail($id);
      $groups = TowerGroups::all()->pluck('title','id');
      $officers = User::where('role_id',3)->pluck('name','id');
      return view('towers.edit',compact('tower','groups','officers'));
    }
  }

  public function update(Request $request,$id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff() || Auth::user()->isFieldOfficer())
    {
      $tower = Towers::findOrFail($id);
      if($request->last_repaired >= $tower->last_repaired)
      {
        $tower->title = $request->title;
        $tower->group_id = $request->group_id;
        $tower->description = $request->description;
        $tower->status = $request->status;
        $tower->last_repaired = $request->last_repaired;
        $tower->next_repair = $request->next_repair;
        $tower->assigned_to = $request->assigned_to;
        $tower->address = $request->address;
        $tower->add_lat = $request->add_lat;
        $tower->add_lng = $request->add_lng;
        $tower->update();
        if($request->status <= 25)
        {
          $assigned_to = $tower->assigned_to;
          $users = User::where('id','=', $assigned_to)->get();
          Notification::send($users, new TowerRepairNotification($tower));
          return redirect()->route('towers.index')->with('success','Tower has been updated successfully and the field officer has been informed about the repair');
        }
      }
      else
      {
        return redirect()->back()->with('errors','Please select the last repaired date later than the last date');
      }
      return redirect()->route('towers.index')->with('success','Tower has been added successfully');
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to access this module');
    }
  }

  public function show($id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff() || Auth::user()->isFieldOfficer())
    {
      $tower = Towers::findOrFail($id);
      $lat = Towers::where('id',$id)->pluck('add_lat')->first();
      $lng = Towers::where('id',$id)->pluck('add_lng')->first();
      $client = new Client();
      $api = $client->get('https://api.openweathermap.org/data/2.5/forecast?lat='.$lat.'&lon='.$lng.'&APPID=37d0aeec47b7e54e2ac32775f48d4eb3&cnt=10');
      $api_body = json_decode($api->getBody(),true);
      //dd($api_body);
      return view('towers.show',compact('tower','api_body'));
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to access this module');
    }
  }

  public function destroy($id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff())
    {
      $tower = Towers::findOrFail($id);
      $tower->delete();
      return redirect()->route('towers.index')->with('success','Tower has been deleted successfully');
    }
    else
    {
      return redirect()->back()->with('errors','You are not allowed to access this module');
    }
  }

  public function notification($tower_id)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff())
    {
      $tower = Towers::findOrFail($tower_id);
      $assigned_to = $tower->assigned_to;
      $users = User::where('id','=', $assigned_to)->get();
      Notification::send($users, new TowerStatusNotification($tower));
      return redirect()->route('towers.index')->with('success','Notification Sent Successfully');
    }
    else
    {
      return redirect()->back()->with('errors','Error');
    }
  }

  public function weather_notification($tower_id,$weather_main,$weather_desc)
  {
    if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff())
    {
      $tower = Towers::findOrFail($tower_id);
      $assigned_to = $tower->assigned_to;
      $users = User::where('id','=', $assigned_to)->get();
      Notification::send($users, new TowerWeatherStatusNotification($tower,$weather_main,$weather_desc));
      return redirect()->route('towers.index')->with('success','Notification Sent Successfully');
    }
    else
    {
      return redirect()->back()->with('errors','Error');
    }
  }

}

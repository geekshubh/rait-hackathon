<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTowersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return
      [
        'title'         =>  'required',
        'group_id'      =>  'required',
        'status'        =>  'required',
        'last_repaired' =>  'required',
        'next_repair'   =>  'required',
        'address'       =>  'required',
        'add_lat'       =>  'required',
        'add_lng'       =>  'required',
        'assigned_to'   =>  'required',
      ];
    }
}

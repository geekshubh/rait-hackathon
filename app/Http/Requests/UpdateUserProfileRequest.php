<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserProfileRequest extends FormRequest
{
  public function authorize()
  {
    return true;
  }
  public function rules()
  {
    return
    [
      'name'          => 'required',
      'email'         => 'required|email|unique:users,email,'.$this->route('user'),
      'phone_number'  => 'numeric|digits:10',
      'profile_pic'   => 'max:10000|mimes:png,jpeg',
    ];
  }

  public function messages()
  {
    return
    [
      'name.required'           =>  trans('validation.name'),
      'email.required'          =>  trans('validation.email'),
      'email.unique'            =>  trans('validation.email_unique'),
      'phone_number.numeric'    =>  trans('validation.phone_number_numeric'),
      'phone_number.digits'     =>  trans('validation.phone_number_digits'),
      'profile_pic.max'         =>  trans('validation.profile_pic_max_size'),
      'profile_pic.mimes'       =>  trans('validation.profile_pic_file_type'),
    ];
  }
}

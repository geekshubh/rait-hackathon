<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCsvImportRequest extends FormRequest
{

  public function authorize()
  {
    return true;
  }

  public function rules()
  {
    return
    [
      'csv_file' => 'required|mimes:csv'
    ];
  }

  public function messages()
  {
    return
    [
      'csv_file.required' => trans('validation.csv_file_required'),
      'csv_file.mimes' => trans('validation.csv_file_required'),
    ];
  }
}

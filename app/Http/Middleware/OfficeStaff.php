<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\Auth;

class OfficeStaff
{
    public function handle($request, Closure $next, $guard = null)
    {
        if (!Auth::user()->isOfficeStaff()) {
            return redirect()->back();
        }

        return $next($request);
    }
}

@extends('layouts.app')
@section('content')
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-lg-3 col-md-6">
        <div class="card">
          <div class="card-body">
            <div class="stat-widget-five">
              <div class="stat-icon dib flat-color-1">
                <i class="pe-7s-map-marker"></i>
              </div>
              <div class="stat-content">
                <div class="text-left dib">
                  <div class="stat-text"><span class="count">{{ $towers }}</span></div>
                  <div class="stat-heading">Towers</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6">
        <div class="card">
          <div class="card-body">
            <div class="stat-widget-five">
              <div class="stat-icon dib flat-color-2">
                <i class="pe-7s-albums"></i>
              </div>
              <div class="stat-content">
                <div class="text-left dib">
                  <div class="stat-text"><span class="count">{{ $tower_groups }}</span></div>
                  <div class="stat-heading">Tower Groups</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6">
        <div class="card">
          <div class="card-body">
            <div class="stat-widget-five">
              <div class="stat-icon dib flat-color-3">
                <i class="pe-7s-tools"></i>
              </div>
              <div class="stat-content">
                <div class="text-left dib">
                  <div class="stat-text"><span class="count">{{ $components }}</span></div>
                  <div class="stat-heading">Components</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6">
        <div class="card">
          <div class="card-body">
            <div class="stat-widget-five">
              <div class="stat-icon dib flat-color-4">
                <i class="pe-7s-users"></i>
              </div>
              <div class="stat-content">
                <div class="text-left dib">
                  <div class="stat-text"><span class="count">{{ $users }}</span></div>
                  <div class="stat-heading">Users</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class ="row">
      <div class ="col-12">
        <div class ="card">
          <h3 class="card-header">Critical Towers</h3>
          <div class="card-body">
              <table id="bootstrap-data-table" class="table table-striped table-bordered" >
                <thead class="bg-light text-capitalize">
                  <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Assigned To</th>
                    <th>Options</th>
                  </tr>
                </thead>
                <tbody>
                  @if (count($critical_towers) > 0)
                  @foreach ($critical_towers as $tower)
                  <tr>
                    <td>{{ $tower->id }}</td>
                    <td>{{ $tower->title }}</td>
                    <td>{{ $tower->status }}</td>
                    <td>{{ $tower->assigned_to_user->name }}</td>
                    <td>
                      @if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff())
                      {!! Form::open(array('style' => 'display: inline-block;','method' => 'POST','route' => ['towers.notification', $tower->id])) !!}
                      {!! Form::submit("Send Notification", array('class' => 'btn btn-md mr-2 mb-2 mt-2 btn-danger')) !!}
                      {!! Form::close() !!}
                      @endif
                      <a href="{{ route('components.index',[$tower->id])}}" class="btn btn-md mr-2 mb-2 mt-2 btn-info"> View All Components</a>
                      <a href="{{ route('towers.show',[$tower->id]) }}" class="btn btn-md mr-2 mb-2 mt-2 btn-primary">View Tower</a>
                      <a href="{{ route('towers.edit',[$tower->id]) }}" class="btn btn-md mr-2 mb-2 mt-2 btn-info">Edit Tower</a>
                    </td>
                  </tr>
                  @endforeach
                  @else
                  <tr>
                    <td colspan="7">@lang('translate.no_entries')</td>
                  </tr>
                  @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class ="row">
      <div class ="col-12">
        <div class ="card">
          <h3 class="card-header">Critical Components</h3>
          <div class="card-body">
              <table id="home-critical-components-table" class="table table-striped table-bordered">
                <thead class="bg-light text-capitalize">
                  <tr>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Last Replaced On</th>
                    <th>Status</th>
                    <th>Options</th>
                  </tr>
                </thead>
                <tbody>
                  @if (count($critical_components) > 0)
                  @foreach ($critical_components as $component)
                  <tr>
                    <td>{{ $component->title }}</td>
                    <td>{{ $component->description }}</td>
                    <td>{{ $component->last_replaced }}</td>
                    <td>{{ $component->status }}</td>
                    <td>
                      {!! Form::open(array('style' => 'display: inline-block;','method' => 'POST','route' => ['components.notification', $component->tower_id,$component->id])) !!}
                      {!! Form::submit("Send Status Notification", array('class' => 'btn btn-md mr-2 mb-2 mt-2 btn-danger')) !!}
                      {!! Form::close() !!}
                      <a href="{{ route('components.show',[$component->tower_id,$component->id]) }}" class="btn btn-md mr-2 mb-2 mt-2 btn-primary">View Component</a>
                      <a href="{{ route('components.edit',[$component->tower_id,$component->id]) }}" class="btn btn-md mr-2 mb-2 mt-2 btn-info">Edit Component</a>
                    </td>
                  </tr>
                  @endforeach
                  @else
                  <tr>
                    <td colspan="7">@lang('translate.no_entries')</td>
                  </tr>
                  @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@include('partials.javascripts')
@include('partials.datatablejs')
@endsection

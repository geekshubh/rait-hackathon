@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>Tower Groups</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="{{ route('dashboard') }}">Home</a></li>
              <li><a href="{{route('tower-groups.index')}}">Tower Groups</a></li>
              <li class="active">View Group</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <h3 class="card-header">Tower Groups
      <a href="{{ route('tower-groups.index') }}" class="btn btn-success btn-md mr-2 float-right">Back to List</a>
    </h3>
    <div class="card-body">
      <table class="table table-bordered table-striped">
        <tr>
          <th>Unique ID</th>
          <td>{!! $group->uid !!}</td>
        </tr>
        <tr>
          <th>Title</th>
          <td>{!! $group->title !!}</td>
        </tr>
        <tr>
          <th>Description</th>
          <td>{{ $group->description }}</td>
        </tr>
      </table>
    </div>
  </div>
</div>
@include('partials.javascripts')
@include('partials.select2js')
@include('partials.datetimepickerjs')
@stop

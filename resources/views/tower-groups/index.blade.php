@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="{{ route('dashboard') }}">Home</a></li>
              <li class="active">Tower Groups</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-xs-12 col-sm-12">
        <div class="card">
          <h3 class="card-header">Tower Groups
            <a href="{{ route('tower-groups.create') }}" class="btn btn-success btn-md mr-2 float-right">Add Tower Group</a>
          </h3>
          <div class="card-body">
            <div class="table-responsive">
              <table id="example" class="table table-striped table-bordered second" style="width:100%">
                <thead class="bg-light text-capitalize">
                  <tr>
                    <th>ID</th>
                    <th>UID</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Options</th>
                  </tr>
                </thead>
                <tbody>
                  @if (count($groups) > 0)
                  @foreach ($groups as $group)
                  <tr>
                    <td>{{ $group->id }}</td>
                    <td>{{ $group->uid }}</td>
                    <td>{{ $group->title }}</td>
                    <td>{{ $group->description }}</td>
                    <td>
                      <a href="{{ route('tower-groups.show',[$group->id]) }}" class="btn btn-md mr-2 mb-2 mt-2 btn-primary">View Tower Group</a>
                      <a href="{{ route('tower-groups.edit',[$group->id]) }}" class="btn btn-md mr-2 mb-2 mt-2 btn-info">Edit Tower</a>
                      {!! Form::open(array('style' => 'display: inline-block;','method' => 'DELETE','onsubmit' => "return confirm('".trans("translate.are_you_sure")."');",'route' => ['tower-groups.destroy', $group->id])) !!}
                      {!! Form::submit(trans('translate.delete'), array('class' => 'btn btn-md mr-2 mb-2 mt-2 btn-danger')) !!}
                      {!! Form::close() !!}
                    </td>
                  </tr>
                  @endforeach
                  @else
                  <tr>
                    <td colspan="7">@lang('translate.no_entries')</td>
                  </tr>
                  @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@include('partials.javascripts')
@include('partials.datatablejs')
@stop

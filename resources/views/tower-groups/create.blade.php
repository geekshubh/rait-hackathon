@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>Tower Groups</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="{{ route('dashboard') }}">Home</a></li>
              <li><a href="{{route('tower-groups.index')}}">Tower Groups</a></li>
              <li class="active">Create</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <h3 class="card-header">Add Tower Group
      <a href="{{ route('tower-groups.index') }}" class="btn btn-danger btn-md mr-2 float-right">Back to List</a>
    </h3>
    <div class="card-body">
      <br>
      {!! Form::open(['method' => 'POST', 'route' => ['tower-groups.store'] ,'enctype'=>'multipart/form-data']) !!}
      <div class="form-group">
        <h6>UID</h6>
        {!! Form::text('uid', old('uid'), ['class' => 'form-control', 'placeholder' => '']) !!}
        @if($errors->has('uid'))
        <br>
        <div class="alert alert-danger">
          <strong>{{ $errors->first('uid') }}</strong>
        </div>
        @endif
      </div>
      <br>
      <div class="form-group">
        <h6>Title</h6>
        {!! Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' => '']) !!}
        @if($errors->has('title'))
        <br>
        <div class="alert alert-danger">
          <strong>{{ $errors->first('title') }}</strong>
        </div>
        @endif
      </div>
      <br>
      <div class="form-group">
        <h6>Description</h6>
        {!! Form::text('description', old('description'), ['class' => 'form-control', 'placeholder' => '']) !!}
        @if($errors->has('description'))
        <br>
        <div class="alert alert-danger">
          <strong>{{ $errors->first('description') }}</strong>
        </div>
        @endif
      </div>
      <br>
      {!! Form::submit(trans('translate.save'), ['class' => 'btn btn-success']) !!}
      {!! Form::close() !!}
    </div>
  </div>
</div>
</div>
@include('partials.javascripts')
@include('partials.select2js')
@include('partials.datetimepickerjs')
@stop

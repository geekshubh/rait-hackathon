@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>Towers</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="{{ route('dashboard') }}">Home</a></li>
              <li><a href="{{ route('towers.index') }}">Towers</a></li>
              <li class="active">Edit</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <h3 class="card-header">Edit Tower
      <a href="{{ route('towers.index') }}" class="btn btn-danger btn-md mr-2 float-right">Back to List</a>
    </h3>
    <div class="card-body">
      <br>
      {!! Form::model($tower, ['method' => 'PUT', 'route' => ['towers.update',$tower->id] ,'enctype'=>'multipart/form-data']) !!}
      <div class="form-group">
        <h6>Tower Group</h6>
        <br>
        {!! Form::select('group_id',$groups ,old('uid'), ['class' => 'form-control']) !!}

      </div>
      <br>
      <div class="form-group">
        <h6>Title</h6>
        <br>
        {!! Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' => '']) !!}

      </div>
      <br>
      <div class="form-group">
        <h6>Description</h6>
        <br>
        {!! Form::text('description', old('description'), ['class' => 'form-control', 'placeholder' => '']) !!}

      </div>
      <br>
      <div class="form-group">
        <h6>Status</h6>
        <br>
        {!! Form::text('status', old('status'), ['class' => 'form-control', 'placeholder' => '']) !!}

      </div>
      <br>
      <div class="form-group">
        <h6>Last Repaired On</h6>
        <br>
        <div class="input-group date" id="classroom-create-start-date" data-target-input="nearest">
          {!! Form::text('last_repaired',old('last_repaired'),['class' =>'form-control datetimepicker-input','data-target'=>'#classroom-create-start-date']) !!}
          <div class="input-group-append" data-target="#classroom-create-start-date" data-toggle="datetimepicker">
            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>

      </div>
      <br>
      <div class="form-group">
        <h6>Next Repair On</h6>
        <br>
        <div class="input-group date" id="classroom-create-end-date" data-target-input="nearest">
          {!! Form::text('next_repair',old('next_repair'),['class' =>'form-control datetimepicker-input','data-target'=>'#classroom-create-end-date']) !!}
          <div class="input-group-append" data-target="#classroom-create-end-date" data-toggle="datetimepicker">
            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>

      </div>
      <div class="form-group">
        <h4>Location</h4>
        <p id="demo" style="visibility: hidden;"></p>
        <div id="map"></div>
        <br><br>
        <a class="btn btn-success btn-md" onclick="getLocation()">Set Location of the tower</a>
        <br><br>
        <h6>Latitude</h6>
        <br>
        {!! Form::text('add_lat',old('add_lat'),['class'=>'form-control','id'=>'lat']) !!}
        <h6>Longitude</h6>
        <br>
        {!! Form::text('add_lng',old('add_lng'),['class'=>'form-control','id'=>'lng']) !!}

      </div>
      <br>
      <div class="form-group">
        <h6>Address</h6>
        <br>
        {!! Form::text('address',old('address'),['class'=>'form-control','id'=>'address']) !!}

      </div>
      <br>
      <div class="form-group">
        <h6>Assigned To</h6>
        <br>
        {!! Form::select('assigned_to',$officers,old('assigned_to'),['class'=>'form-control','id'=>'address']) !!}

      </div>
      <br>
      {!! Form::submit(trans('translate.save'), ['class' => 'btn btn-success']) !!}
      {!! Form::close() !!}
    </div>
  </div>
</div>
@include('partials.javascripts')
@include('partials.select2js')
@include('partials.datetimepickerjs')
@include('partials.mapjs')
@stop

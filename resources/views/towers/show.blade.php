@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>Towers</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="{{ route('dashboard' )}}">Home</a></li>
              <li><a href="{{ route('towers.index') }}">Towers</a></li>
              <li class="active">View Tower</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <h3 class="card-header">Towers
      <a href="{{ route('towers.index') }}" class="btn btn-success btn-md mr-2 float-right">Back to List</a>
    </h3>
    <div class="card-body">
      <table class="table table-bordered table-striped">
        <tr>
          <th>Tower Group</th>
          <td>{{ $tower->groups->title }}</td>
        </tr>
        <tr>
          <th>Title</th>
          <td>{!! $tower->title !!}</td>
        </tr>
        <tr>
          <th>Description</th>
          <td>{{ $tower->description }}</td>
        </tr>
        <tr>
          <th>Status</th>
          <td>{{ $tower->status }}</td>
        </tr>
        <tr>
          <th>Last Repaired On</th>
          <td>{{ $tower->last_repaired }}</td>
        </tr>
        <tr>
          <th>Next Repair On</th>
          <td>{{ $tower->next_repair }}</td>
        </tr>
        <tr>
          <th>Assigned To</th>
          <td>{{ $tower->assigned_to_user->name }}</td>
        </tr>
        <tr>
          <th>View Location</th>

          <td><a href="https://www.google.com/maps/search/?api=1&query={{$tower->add_lat}},{{$tower->add_lng}}" target="_blank" class="btn btn-sm btn-info">View in Google Maps</a></td>
        </tr>
      </table>
      <br><br><br>
      <h4>Weather</h4>
      <br>
      <table class="table table-bordered table-striped">
      @foreach($api_body['list'] as $body)
      <tr>
      <th>{{ $body['dt_txt'] }}</th>
        @foreach($body['weather'] as $weather)
        <td>{{ $weather['main']}}</td>
        <td>{{ $weather['description'] }}</td>
        @php
        $weather_main = $weather['main'];
        $weather_desc = $weather['description'];
        @endphp
        <td>
          {!! Form::open(array('style' => 'display: inline-block;','method' => 'POST','route' => ['towers.weather-notification', $tower->id,$weather_main,$weather_desc])) !!}
          {!! Form::submit("Send Notification", array('class' => 'btn btn-md mr-2 mb-2 mt-2 btn-danger')) !!}
          {!! Form::close() !!}
        </td>
        @endforeach
      <tr>
      @endforeach
    </table>
    </div>
  </div>
</div>

@include('partials.javascripts')
@include('partials.mapjs-show')
@include('partials.weatherjs')
@stop

@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="{{ route('dashboard')}}">Home</a></li>
              <li class="active">Towers</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-xs-12 col-sm-12">
        <div class="card">
          <h3 class="card-header">Towers
            @if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff())
            <a href="{{ route('towers.create') }}" class="btn btn-success btn-md mr-2 float-right">Add Tower</a>
            @endif
          </h3>
          <div class="card-body">
            <div class="table-responsive">
              <table id="example" class="table table-striped table-bordered second" style="width:100%">
                <thead class="bg-light text-capitalize">
                  <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Assigned To</th>
                    <th>Options</th>
                  </tr>
                </thead>
                <tbody>
                  @if (count($towers) > 0)
                  @foreach ($towers as $tower)
                  <tr>
                    <td>{{ $tower->id }}</td>
                    <td>{{ $tower->title }}</td>
                    <td>{{ $tower->status }}</td>
                    <td>{{ $tower->assigned_to_user->name }}</td>
                    <td>
                      @if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff())
                      {!! Form::open(array('style' => 'display: inline-block;','method' => 'POST','route' => ['towers.notification', $tower->id])) !!}
                      {!! Form::submit("Send Notification", array('class' => 'btn btn-md mr-2 mb-2 mt-2 btn-danger')) !!}
                      {!! Form::close() !!}
                      @endif
                      <a href="{{ route('components.index',[$tower->id])}}" class="btn btn-md mr-2 mb-2 mt-2 btn-info"> View All Components</a>
                      <a href="{{ route('towers.show',[$tower->id]) }}" class="btn btn-md mr-2 mb-2 mt-2 btn-primary">View Tower</a>
                      <a href="{{ route('towers.edit',[$tower->id]) }}" class="btn btn-md mr-2 mb-2 mt-2 btn-info">Edit Tower</a>
                      @if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff())
                      {!! Form::open(array('style' => 'display: inline-block;','method' => 'DELETE','onsubmit' => "return confirm('".trans("translate.are_you_sure")."');",'route' => ['towers.destroy', $tower->id])) !!}
                      {!! Form::submit(trans('translate.delete'), array('class' => 'btn btn-md mr-2 mb-2 mt-2 btn-danger')) !!}
                      {!! Form::close() !!}
                      @endif
                    </td>
                  </tr>
                  @endforeach
                  @else
                  <tr>
                    <td colspan="7">@lang('translate.no_entries')</td>
                  </tr>
                  @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@include('partials.javascripts')
@include('partials.datatablejs')
@stop

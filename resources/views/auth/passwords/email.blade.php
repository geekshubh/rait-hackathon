@extends('layouts.auth')
<body class="fp-page">
  <div class="fp-box">
    <div class="logo">
      <a href="javascript:void(0);">E-ducate</b></a>
    </div>
    <div class="card">
      <div class="body">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
          <strong>Whoops!</strong> There were problems with input:
          <br><br>
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
        @endif
        <form id="forgot_password" role="form" method="POST" action="{{ url('password/email') }}">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="msg">Enter your email address that you used to register. We'll send you an email with a link to reset your password.</div>
          <div class="input-group">
            <span class="input-group-addon">
              <i class="material-icons">email</i>
            </span>
            <div class="form-line">
              <input type="email" class="form-control" name="email" value="{{ old('email') }}">
            </div>
          </div>
          <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">RESET MY PASSWORD</button>
          <div class="row m-t-20 m-b--5 align-center">
            <a href="{{ route('auth.login')}}">Sign In!</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</body>

@extends('layouts.app')
@section('content')
<main class="app-content">
  <div class="app-title">
    <div>
      <h1><i class="fa fa-dashboard"></i>@lang('translate.User_actions')</h1>
    </div>
    <ul class="app-breadcrumb breadcrumb">
      <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
      <li class="breadcrumb-item">@lang('translate.User_actions')</li>
    </ul>
  </div>
  <div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
      <div class="card">
        <h3 class="card-header">@lang('translate.User_actions')</h3>
        <div class="card-body">
          <div class="table-responsive">
            <table id="example" class="table table-striped table-bordered second" style="width:100%">
              <thead class="bg-light text-capitalize">
                <tr>
                  <th>@lang('translate.user')</th>
                  <th>@lang('translate.user-actions.action_id')</th>
                  <th>@lang('translate.user-actions.action_model')</th>
                  <th>@lang('translate.user-actions.action')</th>
                </tr>
              </thead>
              <tbody>
                @if (count($user_actions) > 0)
                @foreach ($user_actions as $user_action)
                <tr>
                  <td>{{ $user_action->user->name}}</td>
                  <td>{{ $user_action->action_id }}</td>
                  <td>{{ $user_action->action_model }}</td>
                  <td>{{ $user_action->action }}</td>
                </tr>
                @endforeach
                @else
                <tr>
                  <td colspan="4">@lang('translate.no_entries')</td>
                </tr>
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
@include('partials.javascripts')
@include('partials.datatablejs')
@stop

@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>Components</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="{{ route('dashboard') }}">Home</a></li>
              <li><a href="{{ route('towers.index') }}">Towers</a></li>
              <li class="active">Components</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
   <div class="animated fadeIn">
     <h3 class="card-header">Components
      <a href="{{ route('components.index',[$tower_id]) }}" class="btn btn-success btn-md mr-2 float-right">Back to List</a>
    </h3>
    <div class="card-body">
      <table class="table table-bordered table-striped">
        <tr>
          <th>Tower ID</th>
          <td>{!! $component->tower_id !!}</td>
        </tr>
        <tr>
          <th>Title</th>
          <td>{!! $component->title !!}</td>
        </tr>
        <tr>
          <th>Description</th>
          <td>{!! $component->description !!}</td>
        </tr>
        <tr>
          <th>Last Replaced on</th>
          <th>{!! $component->last_replaced !!}
        </tr>
        <tr>
          <th>Operational Date</th>
          <th>{!! $component->operational_date !!}</th>
        </tr>
        <tr>
          <th>Expiry Date</th>
          <th>{!! $component->expiry_date !!}</th>
        </tr>
        <tr>
          <th>Status</th>
          <th>{!! $component->status !!}</th>
        </tr>
      </table>
    </div>
  </div>
</div>
@include('partials.javascripts')
@include('partials.select2js')
@include('partials.datetimepickerjs')
@stop

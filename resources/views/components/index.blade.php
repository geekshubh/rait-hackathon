@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="{{ route('dashboard')}}">Home</a></li>
              <li><a href="{{ route('towers.index')}}">Towers</a></li>
              <li class="active">Components</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-xs-12 col-sm-12">
        <div class="card">
          <h3 class="card-header">Components
            @if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff())
            <a href="{{ route('components.create',[$tower_id]) }}" class="btn btn-success btn-md mr-2 float-right">Add Component</a>
            @endif
          </h3>
          <div class="card-body">
            <div class="table-responsive">
              <table id="example" class="table table-striped table-bordered second" style="width:100%">
                <thead class="bg-light text-capitalize">
                  <tr>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Last Replaced On</th>
                    <th>Status</th>
                    <th>Options</th>
                  </tr>
                </thead>
                <tbody>
                  @if (count($components) > 0)
                  @foreach ($components as $component)
                  <tr>
                    <td>{{ $component->title }}</td>
                    <td>{{ $component->description }}</td>
                    <td>{{ $component->last_replaced }}</td>
                    <td>{{ $component->status }}</td>
                    <td>
                      @if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff())
                      {!! Form::open(array('style' => 'display: inline-block;','method' => 'POST','route' => ['components.notification', $tower_id,$component->id])) !!}
                      {!! Form::submit("Send Notification", array('class' => 'btn btn-md mr-2 mb-2 mt-2 btn-danger')) !!}
                      {!! Form::close() !!}
                      @endif
                      <a href="{{ route('components.show',[$tower_id,$component->id]) }}" class="btn btn-md mr-2 mb-2 mt-2 btn-primary">View Component</a>
                      <a href="{{ route('components.edit',[$tower_id,$component->id]) }}" class="btn btn-md mr-2 mb-2 mt-2 btn-info">Edit Component</a>
                      @if(Auth::user()->isAdmin() || Auth::user()->isOfficeStaff())
                      {!! Form::open(array('style' => 'display: inline-block;','method' => 'DELETE','onsubmit' => "return confirm('".trans("translate.are_you_sure")."');",'route' => ['components.destroy', $tower_id,$component->id])) !!}
                      {!! Form::submit(trans('translate.delete'), array('class' => 'btn btn-md mr-2 mb-2 mt-2 btn-danger')) !!}
                      {!! Form::close() !!}
                      @endif
                    </td>
                  </tr>
                  @endforeach
                  @else
                  <tr>
                    <td colspan="7">@lang('translate.no_entries')</td>
                  </tr>
                  @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@include('partials.javascripts')
@include('partials.datatablejs')
@stop

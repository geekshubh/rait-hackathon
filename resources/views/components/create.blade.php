@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>Towers</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="{{ route('dashboard') }}">Home</a></li>
              <li><a href="{{ route('towers.index') }}">Towers</a></li>
              <li><a href="{{ route('components.index',[$tower_id]) }}">Components</a></li>
              <li class="active">Add</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <h3 class="card-header">Add Component
      <a href="{{ route('components.index',[$tower_id]) }}" class="btn btn-danger btn-md mr-2 float-right">Back to List</a>
    </h3>
    <div class="card-body">
      <br>
      {!! Form::open(['method' => 'POST', 'route' => ['components.store',$tower_id]]) !!}
      <br>
      <div class="form-group">
        <h6>Title</h6>
        {!! Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' => '']) !!}
        @if($errors->has('title'))
        <br>
        <div class="alert alert-danger">
          <strong>{{ $errors->first('title') }}</strong>
        </div>
        @endif
      </div>
      <br>
      <div class="form-group">
        <h6>Description</h6>
        {!! Form::text('description', old('description'), ['class' => 'form-control', 'placeholder' => '']) !!}
        @if($errors->has('description'))
        <br>
        <div class="alert alert-danger">
          <strong>{{ $errors->first('description') }}</strong>
        </div>
        @endif
      </div>
      <br>
      <div class="form-group">
        <h6>Status</h6>
        {!! Form::text('status', old('status'), ['class' => 'form-control', 'placeholder' => '']) !!}
        @if($errors->has('status'))
        <br>
        <div class="alert alert-danger">
          <strong>{{ $errors->first('status') }}</strong>
        </div>
        @endif
      </div>
      <br>
      <div class="form-group">
        <h6>Last Replaced On</h6>
        <div class="input-group date" id="classroom-create-start-date" data-target-input="nearest">
          {!! Form::text('last_replaced',old('last_replaced'),['class' =>'form-control datetimepicker-input','data-target'=>'#classroom-create-start-date']) !!}
          <div class="input-group-append" data-target="#classroom-create-start-date" data-toggle="datetimepicker">
            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
      </div>
      <br>
      <div class="form-group">
        <h6>Operational Date</h6>
        <div class="input-group date" id="classroom-create-end-date" data-target-input="nearest">
          {!! Form::text('operational_date',old('operational_date'),['class' =>'form-control datetimepicker-input','data-target'=>'#classroom-create-end-date']) !!}
          <div class="input-group-append" data-target="#classroom-create-end-date" data-toggle="datetimepicker">
            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
      </div>
      <br>
      <div class="form-group">
        <h6>Expiry Date</h6>
        <div class="input-group date" id="classroom-edit-end-date" data-target-input="nearest">
          {!! Form::text('expiry_date',old('expiry_date'),['class' =>'form-control datetimepicker-input','data-target'=>'#classroom-edit-end-date']) !!}
          <div class="input-group-append" data-target="#classroom-edit-end-date" data-toggle="datetimepicker">
            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
      </div>
      <br>

      {!! Form::submit(trans('translate.save'), ['class' => 'btn btn-success']) !!}
      {!! Form::close() !!}
    </div>
  </div>
</div>
</div>
@include('partials.javascripts')
@include('partials.select2js')
@include('partials.datetimepickerjs')
@stop

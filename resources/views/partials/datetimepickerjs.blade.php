<script src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.1.2/js/tempusdominus-bootstrap-4.min.js" integrity="sha256-z0oKYg6xiLq3yJGsp/LsY9XykbweQlHl42jHv2XTBz4=" crossorigin="anonymous"></script>
<script>
jQuery(function(){
  $('#library-books-create-due-date').datetimepicker({
    format: 'DD-MM-YYYY'
  });
});
$(function(){
  $('#library-books-create-due-date').datetimepicker({
    format: 'DD-MM-YYYY'
  });
});
$(function(){
  $('#assignment-create-deadline').datetimepicker({
    format: 'DD-MM-YYYY HH:mm'
  });
});
$(function(){
  $('#assignment-edit-deadline').datetimepicker({
    format: 'DD-MM-YYYY HH:mm'
  });
});
$(function(){
  $('#classroom-attendance-create-date').datetimepicker({
    format: 'DD-MM-YYYY'
  });
});
$(function(){
  $('#classroom-timetable-date').datetimepicker({
    format: 'DD-MM-YYYY'
  });
});

$(function(){
  $('#exam-timetable-create-start-date').datetimepicker({
    format: 'DD-MM-YYYY'
  });
});
$(function(){
  $('#exam-timetable-create-end-date').datetimepicker({
    format: 'DD-MM-YYYY'
  });
});
$(function(){
  $('#exam-timetable-edit-start-date').datetimepicker({
    format: 'DD-MM-YYYY'
  });
});

$(function(){
  $('#exam-timetable-edit-end-date').datetimepicker({
    format: 'DD-MM-YYYY'
  });
});
$(function(){
  $('#exam-timetable-subjects-create-date').datetimepicker({
    format: 'DD-MM-YYYY'
  });
});
$(function(){
  $('#classroom-create-start-date').datetimepicker({
    format: 'DD-MM-YYYY'
  });
});
$(function(){
  $('#classroom-edit-start-date').datetimepicker({
    format: 'DD-MM-YYYY'
  });
});
$(function(){
  $('#classroom-create-end-date').datetimepicker({
    format: 'DD-MM-YYYY'
  });
});
$(function(){
  $('#classroom-edit-end-date').datetimepicker({
    format: 'DD-MM-YYYY'
  });
});
$(function(){
  $('#classroom-students-create-dob').datetimepicker({
    format: 'DD-MM-YYYY'
  });
});
$(function(){
  $('#classroom-students-edit-dob').datetimepicker({
    format: 'DD-MM-YYYY'
  });
});
$(function(){
  $('#users-create-dob').datetimepicker({
    format: 'DD-MM-YYYY'
  });
});
$(function(){
  $('#users-edit-dob').datetimepicker({
    format: 'DD-MM-YYYY'
  });
});
$(function(){
  $('#users-edit-profile-dob').datetimepicker({
    format: 'DD-MM-YYYY'
  });
});
$(function(){
  $('#exam-timetable-subjects-create-start-time').datetimepicker({
    format: 'LT'
  });
});
$(function(){
  $('#exam-timetable-subjects-create-end-time').datetimepicker({
    format: 'LT'
  });
});

$(function(){
  $('#exam-timetable-subjects-edit-start-time').datetimepicker({
    format: 'LT'
  });
});
$(function(){
  $('#exam-timetable-subjects-edit-end-time').datetimepicker({
    format: 'LT'
  });
});

$(function(){
  $('#exam-timetable-subjects-edit-date').datetimepicker({
    format: 'DD-MM-YYYY'
  });
});
$(function(){
  $('#expenses-create-date').datetimepicker({
    format: 'DD-MM-YYYY'
  });
});
$(function(){
  $('#expenses-edit-date').datetimepicker({
    format: 'DD-MM-YYYY'
  });
});
$(function(){
  $('#classroom-timetable-create-start-time').datetimepicker({
    format: 'LT',
  });
});
$(function(){
  $('#classroom-timetable-edit-start-time').datetimepicker({
    format: 'LT',
  });
});
$(function(){
  $('#classroom-timetable-create-end-time').datetimepicker({
    format: 'LT',
  });
});
$(function(){
  $('#classroom-timetable-edit-end-time').datetimepicker({
    format: 'LT',
  });
});
</script>

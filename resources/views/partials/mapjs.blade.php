<script type="text/javascript">
  var map; //Will contain map object.
  var marker = false; ////Has the user plotted their location marker?
  var x = document.getElementById("demo");
  var set = false;

  function getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition);
    } else {
      x.innerHTML = "Geolocation is not supported by this browser.";
    }
  }

  function showPosition(position) {
      var mark = {lat: position.coords.latitude, lng: position.coords.longitude};
      if (set==false){
      set = true;
      x.innerHTML = map.getBounds()

    marker = new google.maps.Marker({
                  position: mark ,
                  map: map,
                  draggable: true //make it draggable
              });
    google.maps.event.addListener(marker, 'dragend', function(event){

                      markerLocation();
                  });
      }
      else{
          marker.setPosition(mark);
      }

      markerLocation();
      zzom();
  }
  //Function called to initialize / create the map.
  //This is called when the page has loaded.
  function initMap() {

      //The center location of our map.
      var centerOfMap = new google.maps.LatLng(19.215618, 73.238625);

      //Map options.
      var options = {
        center: centerOfMap, //Set center.
        zoom: 10, clickableLabels: false, clickableIcons:false//The zoom value.
      };

      //Create the map object.
      map = new google.maps.Map(document.getElementById('map'), options);

      //Listen for any clicks on the map.
      google.maps.event.addListener(map, 'click', function(event) {
          //Get the location that the user clicked.
          var clickedLocation = event.latLng;
          if(set === true){
              //Create the marker.

              //Marker has already been added, so just change its location.
              marker.setPosition(clickedLocation);

          }
          //Get the marker's location.
          markerLocation();
      });


  }

  //This function will get the marker's current location and then add the lat/long
  //values to our textfields so that we can save the location.
  function markerLocation(){
      var currentLocation = marker.getPosition();
      var add;
      document.getElementById('lat').value = currentLocation.lat(); //latitude
      document.getElementById('lng').value = currentLocation.lng(); //longitude

      var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+currentLocation.lat()+","+currentLocation.lng()+"&key=AIzaSyBGCql0HlN4C_D7B2BcIIhtuFvjrdfvoew";
      jQuery.getJSON(url,function(data){
        document.getElementById('address').value = data.results[0].formatted_address;
  });
  }

  function zzom(){
      var currentLocation = marker.getPosition();
      map.setCenter(currentLocation);
      map.setZoom(17);
  }


  //Load the map when the page has finished loading.
  google.maps.event.addDomListener(window, 'load', initMap);
</script>

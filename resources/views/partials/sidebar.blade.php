@inject('request', 'Illuminate\Http\Request')
<aside id="left-panel" class="left-panel">
  <nav class="navbar navbar-expand-sm navbar-default">
    <div id="main-menu" class="main-menu collapse navbar-collapse">
      <ul class="nav navbar-nav">
        @if(Auth::user()->isAdmin())
        <li class="active">
          <a href="{{ route('dashboard') }}"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
        </li>
        <li>
          <a href="{{ route('tower-groups.index') }}"><i class="menu-icon fa fa-laptop"></i>Tower Groups</a>
        </li>
        <li>
          <a href="{{ route('towers.index') }}"><i class="menu-icon fa fa-laptop"></i>Towers</a>
        </li>
        <li>
          <a href="{{ route('users.index') }}"><i class="menu-icon fa fa-laptop"></i>Users</a>
        </li>
        @endif
        @if(Auth::user()->isOfficeStaff())
        <li class="active">
          <a href="{{ route('dashboard') }}"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
        </li>
        <li>
          <a href="{{ route('tower-groups.index') }}"><i class="menu-icon fa fa-laptop"></i>Tower Groups</a>
        </li>
        <li>
          <a href="{{ route('towers.index') }}"><i class="menu-icon fa fa-laptop"></i>Towers</a>
        </li>
        @endif
        @if(Auth::user()->isFieldOfficer())
        <li class="active">
          <a href="{{ route('dashboard') }}"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
        </li>
        <li>
          <a href="{{ route('towers.index') }}"><i class="menu-icon fa fa-laptop"></i>Towers</a>
        </li>
        @endif
      </ul>
    </div>
  </nav>
</aside>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="{{ url('v5-assets/js/popper.min.js')}}"></script>
<script src="{{ url('v5-assets/js/main.js')}}"></script>
<!-- The javascript plugin to display page loading on top-->
<script src="{{ url('v5-assets/js/plugins/pace.min.js')}}"></script>
<script>
    window._token = '{{ csrf_token() }}';
</script>

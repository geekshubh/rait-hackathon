<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.min.js" integrity="sha256-qoj3D1oB1r2TAdqKTYuWObh01rIVC1Gmw9vWp1+q5xw=" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="{{ url('assets/vendor/datatables/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="{{ url('assets/vendor/datatables/js/buttons.bootstrap4.min.js')}}"></script>
<script>
jQuery(document).ready( function () {
    jQuery('#home-critical-towers-table').DataTable({
      "paging":   false,
      "info":     false,
      "searching":   true,
      "responsive": false,
    });
    jQuery('#home-critical-components-table').DataTable({
      "paging":   false,
      "info":     false,
      "searching":   true,
      "responsive": false,
    });
});
</script>

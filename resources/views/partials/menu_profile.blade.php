<div class="col-lg-6 clearfix">
  <div class="user-profile float-right">
    @if(!empty(Auth::user()->profile_pic))
    <img class="avatar user-thumb" src="storage/profile-pics/{{ Auth::user()->profile_pic }}" alt="avatar">
    @else
    <img class="avatar user-thumb" src="{{ url('new-assets/images/author/avatar.png')}}" alt="avatar">
    @endif
    <h4 class="user-name dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->name }} <i class="fas fa-angle-down"></i></h4>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="{{ route('dashboard')}}">@lang('translate.home')</a>
      <a class="dropdown-item" href="{{ route('users.profile')}}">@lang('translate.Profile')</a>
      <a class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">@lang('translate.logout')</a>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display:none;">
        {{ csrf_field() }}
      </form>
    </div>
  </div>
</div>

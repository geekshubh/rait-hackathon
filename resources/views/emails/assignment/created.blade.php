<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title></title>
  <style type="text/css">
  </style>
</head>
<body style="margin:0; padding:0; background-color:#F2F2F2;">
  <center>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#F2F2F2">
      <tr>
        <tr>
          <td width="600" class="mobile" align="center" valign="top"><h1>E-ducate</h1></td>
        </tr>
        <td align="center" valign="top">
          <table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper" bgcolor="#FFFFFF">
            <tr>
              <td align="center" valign="top">
                <table width="600" cellpadding="0" cellspacing="0" border="0" class="container">
                  <tr>
                    <td width="600" class="mobile" align="left" valign="top"><h3>New Assignment has been added!</h3></td>
                  </tr>
                  <br>
                  <tr>
                    <td width="300" class="mobile" align="left" valign="top">Title</td>
                    <td width="300" class="mobile" align="center" valign="top">{{ $title }}</td>
                  </tr>
                  <br>
                  <tr>
                    <td width="300" class="mobile" align="left" valign="top">Deadline</td>
                    <td width="300" class="mobile" align="center" valign="top">{{ $deadline }}</td>
                  </tr>
                  <br>
                  <tr>
                    <td width="300" class="mobile" align="left" valign="top">Added By</td>
                    <td width="300" class="mobile" align="center" valign="top">{{ $teacher_name }}</td>
                  </tr>
                  <br>
                  <tr>
                    <td width="300" class="mobile" align="left" valign="top">Subject</td>
                    <td width="300" class="mobile" align="center" valign="top">{{ $subject_name }}</td>
                  </tr>
                  <br>
                  <tr>
                    <td width="600" align="left" class="mobile" valign="top" height="44" style="font-family: Arial, sans-serif; font-size:14px; font-weight:bold;">
                      <a href="https://demo.e-ducate.in/assignments" target="_blank" style="background-color: #4CAF50;border: none;color: white;padding: 15px 32px;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;margin: 4px 2px;cursor: pointer;">View Assignments</a>
                    </td>
                  </tr>
                  <br>
                </table>
              </td>
            </tr>
          </table>
          <br>
        </td>
        <tr>
          <td width="600" class="mobile" align="center" valign="top"><h3>&copy; 2018, E-ducate LLP</h3></td>
        </tr>
        <br>
      </tr>
    </table>
  </center>
</body>
</html>

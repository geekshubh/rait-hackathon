@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>Dashboard</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="#">Dashboard</a></li>
              <li><a href="#">Forms</a></li>
              <li class="active">Advanced</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-xs-12 col-sm-12">
        <div class="card">
          <h3 class="card-header">@lang('translate.Users')
            <a href="{{ route('users.create') }}" class="btn btn-success btn-md mr-2 float-right">@lang('translate.users.add_user')</a>
          </h3>
          <div class="card-body">
            <div class="table-responsive">
              <table id="example" class="table table-striped table-bordered second" style="width:100%">
                <thead class="bg-light text-capitalize">
                  <tr>
                    <th>@lang('translate.id')</th>
                    <th>@lang('translate.full_name')</th>
                    <th>@lang('translate.email_address')</th>
                    <th>@lang('translate.users.user_role')</th>
                    <th>@lang('translate.phone_number')</th>
                    <th>@lang('translate.options')</th>
                  </tr>
                </thead>
                <tbody>
                  @if (count($users) > 0)
                  @foreach ($users as $user)
                  <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->role->title }}</td>
                    <td>{{ $user->phone_number }}</td>
                    <td>
                      <a href="{{ route('users.show',[$user->id]) }}" class="btn btn-md mr-2 mb-2 mt-2 btn-primary">@lang('translate.users.view_user')</a>
                      <a href="{{ route('users.edit',[$user->id]) }}" class="btn btn-md mr-2 mb-2 mt-2 btn-info">@lang('translate.users.edit_user')</a>
                      {!! Form::open(array('style' => 'display: inline-block;','method' => 'DELETE','onsubmit' => "return confirm('".trans("translate.are_you_sure")."');",'route' => ['users.destroy', $user->id])) !!}
                      {!! Form::submit(trans('translate.delete'), array('class' => 'btn btn-md mr-2 mb-2 mt-2 btn-danger')) !!}
                      {!! Form::close() !!}
                    </td>
                  </tr>
                  @endforeach
                  @else
                  <tr>
                    <td colspan="7">@lang('translate.no_entries')</td>
                  </tr>
                  @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@include('partials.javascripts')
@include('partials.datatablejs')
@stop

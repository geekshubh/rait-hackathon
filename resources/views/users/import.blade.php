@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>Dashboard</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="#">Dashboard</a></li>
              <li><a href="#">Forms</a></li>
              <li class="active">Advanced</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-xs-12 col-sm-12">
        <div class="card">
          <h3 class="card-header">@lang('translate.users.import')
            <a href="{{ route('users.index') }}" class="btn btn-default btn-danger float-right">@lang('translate.back_to_list')</a>
          </h3>
          <div class="card-body">
            <br>
            <form class="form-horizontal" method="POST" action="{{ route('users.import_fields') }}" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="form-group{{ $errors->has('csv_file') ? ' has-error' : '' }}">
                <label for="csv_file" class="col-md-4 control-label">@lang('translate.csv_file_import')</label>
                <div class="col-md-6">
                  <input id="csv_file" type="file" class="form-control" name="csv_file" required>
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-offset-4 col-md-6 ">
                  <div class="checkbox">
                    <input type="checkbox" name="header" id="md_checkbox_1" class="filled-in chk-col-red" checked />
                    <label for="md_checkbox_1">&nbsp;&nbsp;&nbsp;@lang('translate.file_header_row')</label>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                  <button type="submit" class="btn btn-primary">@lang('translate.parse_csv')</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@include('partials.javascripts')
@endsection

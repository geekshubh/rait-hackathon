@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>Dashboard</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="#">Dashboard</a></li>
              <li><a href="#">Forms</a></li>
              <li class="active">Advanced</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-xs-12 col-sm-12">
        <div class="card">
          <h3 class="card-header">@lang('translate.users.edit_user')
            <a href="{{ route('users.index') }}" class="btn btn-default btn-danger float-right">@lang('translate.back_to_list')</a>
          </h3>
          <div class="card-body">
            <br>
            {!! Form::model($user, ['method' => 'PUT', 'route' => ['users.update', $user->id] ,'enctype'=>'multipart/form-data']) !!}
            <div class="form-group">
              <h6>@lang('translate.full_name')</h6>
              {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '']) !!}
              @if($errors->has('name'))
              <br>
              <div class="alert alert-danger">
                <strong>{{ $errors->first('name') }}</strong>
              </div>
              @endif
            </div>
            <br>
           <div class="form-group">
             <h6>@lang('translate.roll_number')</h6>
             {!! Form::text('roll_no', old('roll_no'), ['class' => 'form-control', 'placeholder' => '']) !!}
             @if($errors->has('roll_no'))
             <br>
             <div class="alert alert-danger">
               <strong>{{ $errors->first('roll_no') }}</strong>
             </div>
             @endif
           </div>
            <br>
            <div class="form-group">
              <h6>@lang('translate.email_address')</h6>
              {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => '']) !!}
              @if($errors->has('email'))
              <br>
              <div class="alert alert-danger">
                <strong>{{ $errors->first('email') }}</strong>
              </div>
              @endif
            </div>
            <br>
            <div class="form-group">
              <h6>@lang('translate.password')</h6>
              {!! Form::password('password', ['class' => 'form-control', 'placeholder' => '']) !!}
              @if($errors->has('password'))
              <br>
              <div class="alert alert-danger">
                <strong>{{ $errors->first('password') }}</strong>
              </div>
              @endif
            </div>
            <br>
            <div class="form-group">
              <h6>@lang('translate.phone_number')</h6>
              {!! Form::number('phone_number',old('phone_number'),['class'=>'form-control']) !!}
              @if($errors->has('phone_number'))
              <br>
              <div class="alert alert-danger">
                <strong>{{ $errors->first('phone_number') }}</strong>
              </div>
              @endif
            </div>
            <br>
            <div class="form-group">
              <h6>@lang('translate.gender')</h6>
              {!! Form::select('gender', $gender, old('gender'), ['class' => 'form-control selectpicker','data-live-search'=>'true']) !!}
            </div>
            <br>
            <div class="form-group">
              <h6>@lang('translate.profile_pic_upload')</h6>
              {{ Form::file('profile_pic',['class'=>'form-control']) }}
              @if($errors->has('profile_pic'))
              <br>
              <div class="alert alert-danger">
                <strong>{{ $errors->first('profile_pic') }}</strong>
              </div>
              @endif
            </div>
            <br>
            <div class="form-group">
              <h6>@lang('translate.users.user_role')</h6>
              {!! Form::select('role_id', $roles, old('role_id'), ['class' => 'form-control selectpicker','data-live-search'=>'true']) !!}
              @if($errors->has('role_id'))
              <br>
              <div class="alert alert-danger">
                <strong>{{ $errors->first('role_id') }}</strong>
              </div>
              @endif
            </div>
            <br>
            {!! Form::submit(trans('translate.save'), ['class' => 'btn btn-success']) !!}
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@include('partials.javascripts')
@include('partials.select2js')
@include('partials.datetimepickerjs')
@stop

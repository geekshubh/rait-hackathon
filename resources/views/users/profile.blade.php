@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>Dashboard</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="#">Dashboard</a></li>
              <li><a href="#">Forms</a></li>
              <li class="active">Advanced</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-xs-12 col-sm-12">
        <div class="card">
          <h3 class="card-header">@lang('translate.Profile')
            <a href="{{ route('users.edit_profile',['id'=>$users->id]) }}" class="btn btn-lg float-right btn-success">@lang('translate.users.edit_profile')</a>
          </h3>
          <div class="card-body">
            <div class="row">
              <div class ="col-sm-12 col-md-3 col-lg-3">
                @if(!empty(Auth::user()->profile_pic))
                <img alt = "profile picture" src = "/storage/profile-pics/{{$users->profile_pic}}" class="img-circle img-responsive" />
                @else
                <img src="{{ url('assets/images/avatar-3.jpg')}}" alt="" class="img-circle img-responsive"></a>
                @endif
              </div>
              <div class ="col-sm-12 col-md-9 col-lg-9">
                <table class="table table-user-information">
                  <tbody>
                    <tr>
                      <td>@lang('translate.full_name')</td>
                      <td>{{ $users->name }}</td>
                    </tr>
                    <tr>
                      <td>@lang('translate.email_address')</td>
                      <td>{{ $users->email }}</td>
                    </tr>
                    <tr>
                      <td>@lang('translate.phone_number')</td>
                      <td>{{ $users->phone_number }}</td>
                    </tr>
                    <tr>
                      <td>@lang('translate.gender')</td>
                      <td>{{ $users->gender }}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@include('partials.javascripts')
@stop

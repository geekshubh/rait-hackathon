@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
    <div class="row m-0">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>Dashboard</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="#">Dashboard</a></li>
              <li><a href="#">Forms</a></li>
              <li class="active">Advanced</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-xs-12 col-sm-12">
        <div class="card">
          <h3 class="card-header">@lang('translate.users.import')
            <a href="{{ route('users.index') }}" class="btn btn-default btn-danger float-right">@lang('translate.back_to_list')</a>
          </h3>
          <div class="card-body">
            <br>
            <form class="form-horizontal" method="POST" action="{{ route('users.import_process') }}">
              {{ csrf_field() }}
              <input type="hidden" name="csv_data_file_id" value="{{ $csv_data_file->id }}" />
              <table class="table table-bordered table-striped table-hover table-responsive table-body">
                @if (isset($csv_header_fields))
                <tr>
                  @foreach ($csv_header_fields as $csv_header_field)
                  <th>{{ $csv_header_field }}</th>
                  @endforeach
                </tr>
                @endif
                @foreach ($csv_data as $row)
                <tr>
                  @foreach ($row as $key => $value)
                  <td>{{ $value }}</td>
                  @endforeach
                </tr>
                @endforeach
                <tr>
                  @foreach ($csv_data[0] as $key => $value)
                  <td>
                    <select name="fields[{{ $key }}]">
                    @foreach (config('import.users_db_fields') as $db_field)
                    <option value="{{ (\Request::has('header')) ? $db_field : $loop->index }}" @if ($key === $db_field) selected @endif>{{ $db_field }}</option>
                    @endforeach
                    </select>
                  </td>
                  @endforeach
                </tr>
              </table>
              <br>
              <button type="submit" class="btn btn-success">@lang('translate.import')</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@include('partials.javascripts')
@endsection

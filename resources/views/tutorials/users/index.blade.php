@extends('layouts.app')
@section('content')
<main class="app-content">
  <div class="app-title">
    <div>
      <h1><i class="fa fa-dashboard"></i>@lang('tutorial.assignments.create.title')</h1>
      <p></p>
    </div>
    <ul class="app-breadcrumb breadcrumb">
      <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
      <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">@lang('translate.tutorial')</a></li>
      <li class="breadcrumb-item"><a href="{{ route('assignments.index') }}">@lang('translate.Assignments')</a></li>
      <li class="breadcrumb-item">@lang('translate.assignment.add_assignment')</li>
    </ul>
  </div>
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
          <div class="card-header">
            <h5>@lang('tutorial.users.index.title')</h5>
            <p>@lang('tutorial.users.index.desc')</p>
          </div>
          <div class="card-body">
            <ol type = 1>
            <img class ="card-img" src="{{ url('/assets/tutorials/users/index.jpg')}}"></img>
            <br>
            <br>
                <li>@lang('tutorial.users.index.1')</li>
                <li>@lang('tutorial.users.index.2')</li>
                <li>@lang('tutorial.users.index.3')</li>
                <li>@lang('tutorial.users.index.4')</li>
                <li>@lang('tutorial.users.index.5')</li>
                <li>@lang('tutorial.users.index.6')</li>
                <li>@lang('tutorial.users.index.7')</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
</main>
@include('partials.javascripts')
@endsection

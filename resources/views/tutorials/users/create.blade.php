@extends('layouts.app')
@section('content')
<main class="app-content">
  <div class="app-title">
    <div>
      <h1><i class="fa fa-dashboard"></i>@lang('tutorial.assignments.create.title')</h1>
      <p></p>
    </div>
    <ul class="app-breadcrumb breadcrumb">
      <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
      <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">@lang('translate.tutorial')</a></li>
      <li class="breadcrumb-item"><a href="{{ route('assignments.index') }}">@lang('translate.Assignments')</a></li>
      <li class="breadcrumb-item">@lang('translate.assignment.add_assignment')</li>
    </ul>
  </div>
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
          <div class="card-header">
            <h5>@lang('tutorial.users.create.title')</h5>
            <p>@lang('tutorial.users.create.desc')</p>
          </div>
          <div class ="card-body">
            <ol type = 1>
              <img class ="card-img" src="{{url('/assets/tutorials/users/full-name.jpg')}}"></img>
              <br><br>
              <li>@lang('tutorial.users.create.1')</li>
              <hr>
              <img class ="card-img" src="{{url('/assets/tutorials/users/roll-number.png')}}"></img>
              <br><br>
              <li>@lang('tutorial.users.create.2')</li>
              <hr>
              <img class ="card-img" src="{{url('/assets/tutorials/users/email-address.png')}}"></img>
              <br><br>
              <li>@lang('tutorial.users.create.3')</li>
              <hr>
              <img class ="card-img" src="{{url('/assets/tutorials/users/password.png')}}"></img>
              <br><br>
              <li>@lang('tutorial.users.create.4')</li>
              <hr>
              <img class ="card-img" src="{{url('/assets/tutorials/users/phone-number.png')}}"></img>
              <br><br>
              <li>@lang('tutorial.users.create.5')</li>
              <hr>
              <img class ="card-img" src="{{url('/assets/tutorials/users/gender.png')}}"></img>
              <br><br>
              <li>@lang('tutorial.users.create.6')</li>
              <hr>
              <img class ="card-img" src="{{url('/assets/tutorials/users/birthdate.png')}}"></img>
              <br><br>
              <li>@lang('tutorial.users.create.7')</li>
              <hr>
              <img class ="card-img" src="{{url('/assets/tutorials/users/address.png')}}"></img>
              <br><br>
              <li>@lang('tutorial.users.create.8')</li>
              <hr>
              <img class ="card-img" src="{{url('/assets/tutorials/users/profile-picture.jpg')}}"></img>
              <br><br>
              <li>@lang('tutorial.users.create.9')</li>
              <hr>
              <img class ="card-img" src="{{url('/assets/tutorials/users/user-role.png')}}"></img>
              <br><br>
              <li>@lang('tutorial.users.create.10')</li>
              <br>
              <li>@lang('tutorial.users.create.11')</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
</main>
@include('partials.javascripts')
@endsection

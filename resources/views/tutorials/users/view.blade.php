@extends('layouts.app')
@section('content')
<main class="app-content">
  <div class="app-title">
    <div>
      <h1><i class="fa fa-dashboard"></i>@lang('tutorial.assignments.create.title')</h1>
      <p></p>
    </div>
    <ul class="app-breadcrumb breadcrumb">
      <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
      <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">@lang('translate.tutorial')</a></li>
      <li class="breadcrumb-item"><a href="{{ route('assignments.index') }}">@lang('translate.Assignments')</a></li>
      <li class="breadcrumb-item">@lang('translate.assignment.add_assignment')</li>
    </ul>
  </div>
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        @include('sweet::alert')
        <div class="card">
        </div>
        </div>
        </div>
</main>
        @include('partials.javascripts')

<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.auth_head')
</head>
<body>
  @yield('content')
  @include('partials.auth_javascripts')
</body>
</html>

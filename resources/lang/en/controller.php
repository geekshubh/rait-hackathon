<?php

return
[
  'user-actions' =>
  [
    'index_error'        =>'You are not allowed to View the Requested Page',
  ],

  'users' =>
  [
    'index_error'        =>'You are not allowed to View the Requested Page',
    'create_error'      =>'You are not allowed to Add User',
    'create_success'    =>'User Successfully Added',
    'edit_error'        =>'You are not allowed to Edit the User',
    'edit_success'      =>'User Successfully Updated',
    'view_error'        =>'You are not allowed to View the User',
    'delete_error'      =>'You are not allowed to Delete the User',
    'import_success'    =>'Users sucessfully imported',
    'import_error'      =>'You are not allowed to Import Users',
  ],
];
?>

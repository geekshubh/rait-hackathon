<?php
return
[
  'table-options' =>
  [
    'copy'   => 'The User can copy the table data and paste it.',
    'excel'  => 'The User can export the table data in excel file.',
    'pdf'    => 'The User can export the table data in PDF file.',
    'print'  => 'The User can print the table data.',
  ],

  'users'        =>
  [
    'create'     =>
    [
      'title'    =>  'Tutorial for Add Page of Users.',
      'desc'     =>  'This page allows the admin to Add users.',
      '1'        =>  'In the Full name Box mention the name of the User.',
      '2'        =>  'In the Roll no. Box mention the roll no. of the User.',
      '3'        =>  'In the Email Address option mention the email address of the user.',
      '4'        =>  'In the Password box mention your password.',
      '5'        =>  'In the Phone number box mention your Phone number.',
      '6'        =>  'In Gender option select your gender.',
      '7'        =>  'In the Birthdate option select your birthdate.',
      '8'        =>  'In the Address text box mention your address.',
      '9'        =>  'In the Profile Picture Upload option upload your profile picture using choose file option.',
      '10'       =>  'In the User role select the appropriate option that describes the role of the user.',
      '11'       =>  'After filling out the necessary details to Add an user, save the user.',
    ],
    'edit'       =>
    [
      'title'    =>  'Tutorial for Edit Page of Users.',
      'desc'     =>  'This page allows the admin to edit users.',
      '1'        =>  'In the Full name Box mention the name of the User.',
      '2'        =>  'In the Roll no. Box mention the roll no. of the User.',
      '3'        =>  'In the Email Address option mention the email address of the user.',
      '4'        =>  'In the Password box mention your password.',
      '5'        =>  'In the Phone number box mention your Phone number.',
      '6'        =>  'In Gender option select your gender.',
      '7'        =>  'In the Birthdate option select your birthdate.',
      '8'        =>  'In the Address text box mention your address.',
      '9'        =>  'In the Profile Picture Upload option upload your profile picture using choose file option.',
      '10'       =>  'In the User role select the appropriate option that describes the role of the user.',
      '11'       =>  'After filling out the necessary details to edit an user, update the user.',
    ],

    'view'       =>
    [
      'title'    =>  'Tutorial for Profile page of Users',
      'desc'     =>  'This page allows the admin to view the profile of other users.',
      '1'        =>  'Full name indicates the name of the user.',
      '2'        =>  'Email Address indicates the email address of the user.',
      '3'        =>  'The Phone number indicates the phone number of the user.',
      '4'        =>  'Gender indicates the gender of the user.',
      '5'        =>  'Birthdate indicates the date of birth of the user.',
      '6'        =>  'Address indicated the address of the user.',
    ],

    'index'      =>
    [
      'title'    =>  'Tutorial for Index Page of Users',
      'desc'     =>  'This page allows the user to view the index of the Users.',
      '1'        =>  'ID column in the table specifies the ID number of the users.',
      '2'        =>  'Full Name column in the table specifies the name of the User. ',
      '3'        =>  'Email Address column in the table specifies the email address of the user.',
      '4'        =>  'The user role column in the table specifies the role of the user.',
      '5'        =>  'Phone number column in the table specifies the phone number of the users.',
      '6'        =>  'The options column allows the admin to view the user profile, edit it, delete it.',
      '7'        =>  'The Add user option allows the admin to add users.',
    ],
  ],
]
?>

<?php

return
[
  'exam_subjects_db_fields' =>
  [
    'timetable_id',
    'subject_id',
    'title',
    'date',
    'start_time',
    'end_time',
    'added_by',
  ],

  'subjects_db_fields' =>
  [
    'title',
    'description',
    'added_by',
  ],

  'expenses_db_fields'=>
  [
    'title',
    'notes',
    'date',
    'amount',
    'expense_category',
    'added_by',
  ],

  'expenses_category_db_fields'=>
  [
    'category_title',
    'category_description',
    'added_by',
  ],

  'classroom_timetable_db_fields'=>
  [
    'classroom_id',
    'date',
    'subject_id',
    'teacher_id',
    'start_time',
    'end_time',
    'added_by',
  ],

  'classroom_students_db_fields'=>
  [
    'name',
    'email',
    'password',
    'phone_number',
    'gender',
    'roll_no',
    'role_id',
    'student_classroom_id',
  ],
  'classroom_teachers_db_fields'=>
  [
    'teacher_id',
    'subject_id',
    'classroom_id',
    'added_by',
  ],

  'classroom_attendance_db_fields'=>
  [
    'attendance_id',
    'student_id',
    'status',
    'added_by',
  ],

  'library_db_fields'=>
  [
    'title',
    'author',
    'type',
    'availability',
    'added_by',
  ],

  'questions_db_fields'=>
  [
    'teacher_id',
    'question_text',
    'answer_explanation',
    'more_info_link',
    'added_by',
  ],

  'questions_options_db_fields'=>
  [
    'question_id',
    'option',
    'correct',
    'added_by',
  ],

  'exam_marks_db_fields'=>
  [
    'timetable_id',
    'subject_id',
    'student_id',
    'marks_obtained',
    'marks_out_off',
    'added_by',
  ],

  'student_parents_db_fields'=>
  [
    'student_id',
    'parent_id',
    'added_by',
  ],
  'users_db_fields'   =>
  [
    'name',
    'email',
    'password',
    'phone_number',
    'roll_no',
    'gender',
    'dob',
    'role_id',

  ],
];
?>

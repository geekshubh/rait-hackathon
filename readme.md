<h1 align="center">Solution for Electricity Board Tower Management</h1>
<p>
There are 3 user roles namely Admin, Officer and the
Field Officer.
The Admin is the Head of the whole operation so they will
be able to add, edit and delete all the users. The Officer
will be able to add, edit and delete the details of the
electricity towers. The Field Officer will be able to update
the ongoing status of the work going on at the towers,
they will also get a notification for when the next repair is
due.</p>
<p>
Each tower will have a distinctive id, title, description and
location which can be tracked. The location can be added with utmost precision
via the in-app Google map, this removes the manual work of adding Longitudes
and Latitudes of the towers. When a certain tower is brought up, all the components
used in the tower is listed. Each component also has a
distinctive id, title, tower id (to showcase which tower it
is being used in), description, operational date, current
status. The Admin/Officer will also be able to see when
was the component last replaced and also its expiry date.
The Admin and the Officer will be able to add, edit and
delete the components but the Field Officer can only
update the operational date, current status, last replaced
date and the expiry date.
</p>
<p>
The Towers are divided into groups which has a distinctive id. This is to
predict which towers will malfunction if one of the towers suffer any technical failure,
as the towers are inter-linked.
</P
<p>
There is also a notification feature which will make sure the field officer
gets a message and an e-mail when a tower needs any maintenance. The notifications
are automated which means whenever a component is about to expire or the tower
malfunctions prior notifications are sent to the respective field officers who operate
in that tower's group.
</p>

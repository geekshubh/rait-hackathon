<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTowersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('towers', function (Blueprint $table) {
          $table->increments('id');
          $table->text('title');
          $table->integer('group_id')->unsigned();
          $table->foreign('group_id')->references('id')->on('users')->onDelete('cascade');
          $table->text('description')->nullable();
          $table->text('status');
          $table->text('last_repaired');
          $table->text('next_repair');
          $table->integer('assigned_to')->nullable()->unsigned();
          $table->foreign('assigned_to')->references('id')->on('users')->onDelete('cascade');
          $table->string('address');
          $table->double('add_lat');
          $table->double('add_lng');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('towers');
    }
}

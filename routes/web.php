<?php


// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('auth.login');
$this->post('login', 'Auth\LoginController@login')->name('auth.login');

Route::group(['middleware' => 'auth'], function () {
  Route::post('towers/{tower_id}/send-weather-notification/{weather_main}/{weather_desc}',['uses'=>'TowersController@weather_notification','as'=>'towers.weather-notification']);
    // Home Route
    Route::get('/', ['uses'=>'HomeController@index','as'=>'dashboard']);
    // Language Route
    Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'LanguageController@switchLang']);

    Route::get('tower-groups',['uses'=>'TowerGroupsController@index','as'=>'tower-groups.index']);
    Route::get('tower-groups/add',['uses'=>'TowerGroupsController@create','as'=>'tower-groups.create']);
    Route::post('tower-groups/store',['uses'=>'TowerGroupsController@store','as'=>'tower-groups.store']);
    Route::get('tower-groups/{id}/edit',['uses'=>'TowerGroupsController@edit','as'=>'tower-groups.edit']);
    Route::put('tower-groups/{id}/update',['uses'=>'TowerGroupsController@update','as'=>'tower-groups.update']);
    Route::get('tower-groups/{id}',['uses'=>'TowerGroupsController@show','as'=>'tower-groups.show']);
    Route::delete('tower-groups/{id}/destroy',['uses'=>'TowerGroupsController@destroy','as'=>'tower-groups.destroy']);

    Route::get('towers',['uses'=>'TowersController@index','as'=>'towers.index']);
    Route::get('towers/add',['uses'=>'TowersController@create','as'=>'towers.create']);
    Route::post('towers/store',['uses'=>'TowersController@store','as'=>'towers.store']);
    Route::get('towers/{id}/edit',['uses'=>'TowersController@edit','as'=>'towers.edit']);
    Route::put('towers/{id}/update',['uses'=>'TowersController@update','as'=>'towers.update']);
    Route::get('towers/{id}',['uses'=>'TowersController@show','as'=>'towers.show']);
    Route::delete('towers/{id}/destroy',['uses'=>'TowersController@destroy','as'=>'towers.destroy']);
    Route::post('towers/{tower_id}/send-notification',['uses'=>'TowersController@notification','as'=>'towers.notification']);

    Route::get('towers/{tower_id}/components',['uses'=>'ComponentsController@index','as'=>'components.index']);
    Route::get('towers/{tower_id}/components/add',['uses'=>'ComponentsController@create','as'=>'components.create']);
    Route::post('towers/{tower_id}/components/store',['uses'=>'ComponentsController@store','as'=>'components.store']);
    Route::get('towers/{tower_id}/components/{id}/edit',['uses'=>'ComponentsController@edit','as'=>'components.edit']);
    Route::put('towers/{tower_id}/components/{id}/update',['uses'=>'ComponentsController@update','as'=>'components.update']);
    Route::get('towers/{tower_id}/components/{id}',['uses'=>'ComponentsController@show','as'=>'components.show']);
    Route::delete('towers/{tower_id}/components/{id}/destroy',['uses'=>'ComponentsController@destroy','as'=>'components.destroy']);
    Route::post('towers/{tower_id}/components/{id}/notification',['uses'=>'ComponentsController@notification','as'=>'components.notification']);

    Route::get('users/import', ['uses'=>'UsersController@getImport','as'=>'users.import']);
    Route::post('users/import_parse', ['uses'=>'UsersController@parseImport','as'=>'users.import_fields']);
    Route::post('users/import_process', ['uses'=>'UsersController@processImport','as'=>'users.import_process']);
    // Users Index Route
    Route::get('users',['uses'=>'UsersController@index','as'=>'users.index']);
    // Add User Route
    Route::get('users/add',['uses'=>'UsersController@create','as'=>'users.create']);
    // Store User Route
    Route::post('users/store',['uses'=>'UsersController@store','as'=>'users.store']);
    // Edit User Route
    Route::get('users/{id}/edit',['uses'=>'UsersController@edit','as'=>'users.edit']);
    // Update User Route
    Route::put('users/{id}/update',['uses'=>'UsersController@update','as'=>'users.update']);
    // Delete User Route
    Route::delete('users/{id}/destroy',['uses'=>'UsersController@destroy','as'=>'users.destroy']);
    // View User Route
    Route::get('users/{id}',['uses'=>'UsersController@show','as'=>'users.show']);

    // User Actions Route
    Route::get('user-actions', ['uses'=>'UserActionsController@index','as'=>'user_actions.index']);
    $this->post('logout', 'Auth\LoginController@logout')->name('logout');
});

<?php

Route::post('user/login', 'API\LoginController@login');
//  Route::post('logout', 'AuthController@logout');
//  Route::post('refresh', 'AuthController@refresh');

Route::group(['middleware'=>['jwt.verify']],function(){
  // Student Parents Import Route
  Route::get('users/import', ['uses'=>'API\UsersController@getImport','as'=>'users-api.import']);
  Route::post('users/import_parse', ['uses'=>'API\UsersController@parseImport','as'=>'users-api.import_fields']);
  Route::post('users/import_process', ['uses'=>'API\UsersController@processImport','as'=>'users-api.import_process']);
  // Users Index Route
  Route::get('users',['uses'=>'API\UsersController@index','as'=>'users-api.index']);
  // Add User Route
  Route::get('users/add',['uses'=>'API\UsersController@create','as'=>'users-api.create']);
  // Store User Route
  Route::post('users/store',['uses'=>'API\UsersController@store','as'=>'users-api.store']);
  // Edit User Route
  Route::get('users/{id}/edit',['uses'=>'API\UsersController@edit','as'=>'users-api.edit']);
  // Update User Route
  Route::put('users/{id}/update',['uses'=>'API\UsersController@update','as'=>'users-api.update']);
  // Delete User Route
  // View User Route
  Route::delete('users/{id}/destroy',['uses'=>'API\UsersController@destroy','as'=>'users-api.destroy']);
  Route::get('users/{id}',['uses'=>'API\UsersController@show','as'=>'users-api.show']);

});
